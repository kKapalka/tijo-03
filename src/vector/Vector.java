/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vector;

/**
 *
 * @author student
 */
public class Vector {
    /** funkcja zwraca wylosowany rozmiar wektora z zakresu <5, 10> */
    public static int generateSizeOfVector() {
        return Math.round((int)(Math.random()*6))+5;
    }
    /**
    * funkcja wprowadza dane do wektora.
    */
    public static void insertDataToVector(double[] vector) {
        int i=0;
        try{
            while(true){
                vector[i++]=Math.random();
            }
        } catch (ArrayIndexOutOfBoundsException ex){
            System.out.println("Złapano wyjątek ArrayIndexOutOfBoundsException");
        } finally{
            System.out.println("Koniec wprowadzania danych");
        }
    }
    /**
    * funkcja wyswietla zawartosc wektora w odpowiednim formacie.
    */
    public static void showVector(double[] vector) {
        int i=0;
        try{
            while(true){
                System.out.format("TAB["+i+"]: "+vector[i++]+"\n");
            }
        } catch (ArrayIndexOutOfBoundsException ex){
            System.out.println("Złapano wyjątek ArrayIndexOutOfBoundsException");
        } finally{
            System.out.println("Koniec wyświetlania danych");
        }
    }
    public static void main(String[] args) {
        System.out.println();
        System.out.println("Generuje rozmiar wektora...");
        double[] vector = new double[generateSizeOfVector()];
        System.out.println("Wprowadzam dane do wektora...");
        insertDataToVector(vector);
        System.out.println("Wyswietlam zawartosc wektora");
        showVector(vector);
    }
}
