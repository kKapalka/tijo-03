/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz2;

/**
 *
 * @author student
 */
public class QuizImpl implements Quiz{
    private int digit;
    public QuizImpl() {
        this.digit = 1000; // w kazdej chwili zmienna moze otrzymac inna wartosc!
    }
    // implementacja metody isCorrectValue...
    @Override
    public void isCorrectValue(int value) throws Quiz.ParamTooLarge, Quiz.ParamTooSmall{
        if(value>digit){
            throw new Quiz.ParamTooLarge();
        }
        else if (value<digit){
            throw new Quiz.ParamTooSmall();
        }
    }
}
